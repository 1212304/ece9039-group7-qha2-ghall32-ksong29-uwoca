# README #

This repository is hosting the Jupyter notebook for Final Project of ECE9039B/9039B: Machine Learning: From Theory to Applications - Winter 2021 - Western University of Ontario.

The project was completed by group 7:

Authors:

- Gabriel Hall
- Quang V. Ha
- Kunpeng Song

The Jupyter notebook was originally from [our shared Google Drive](https://drive.google.com/file/d/125XMW9e8wQLtWxZhZeUODhHKBtiQw5oR/view?usp=sharing).
All code snippets and console output was originally created, edited (including training machine learning models) using Google Colab.

Top 17% record: [here](https://www.kaggle.com/quangvh/competitions?isEditing=False)

Download data: [Kaggle](https://www.kaggle.com/c/house-prices-advanced-regression-techniques/data)

Competition information: [Kaggle](https://www.kaggle.com/c/house-prices-advanced-regression-techniques)